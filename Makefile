#Escriba su makefiile en este archivo
#
# Luiggi Alarcón Gonzabay
# Juan Laso Delgado
FLAGS= gcc -Wall -c -I

bin/prueba: obj/hash.o obj/crearHashtable.o obj/numeroElementos.o obj/put.o obj/get.o obj/remover.o obj/borrar.o obj/claves.o obj/contieneClave.o obj/valores.o obj/prueba.o
	gcc -Wall $^ -o $@ -g
obj/hash.o: src/hash.c
	$(FLAGS) include/ $< -o $@ -g
obj/crearHashtable.o: src/crearHashtable.c
	$(FLAGS) include/ $< -o $@ -g
obj/numeroElementos.o: src/numeroElementos.c
	$(FLAGS) include/ $< -o $@ -g
obj/put.o: src/put.c
	$(FLAGS) include/ $< -o $@ -g
obj/get.o: src/get.c
	$(FLAGS) include/ $< -o $@ -g
obj/remover.o: src/remover.c
	$(FLAGS) include/ $< -o $@ -g
obj/borrar.o: src/borrar.c
	$(FLAGS) include/ $< -o $@ -g
obj/claves.o: src/claves.c
	$(FLAGS) include/ $< -o $@ -g
obj/contieneClave.o: src/contieneClave.c
	$(FLAGS) include/ $< -o $@ -g
obj/valores.o: src/valores.c
	$(FLAGS) include/ $< -o $@ -g

obj/prueba.o: src/prueba.c
	$(FLAGS) include/ $< -o $@ -g
	
.PHONY: clean 	
clean:
	rm bin/prueba

