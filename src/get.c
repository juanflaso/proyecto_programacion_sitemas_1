#include <stdio.h>
#include <string.h>
#include "hashtable.h"

void *get(hashtable *tabla, char *clave) {
    int hashedKey = hash((unsigned char *)clave)%(tabla->numeroBuckets);
    objeto **buckets = tabla->buckets;

    if(buckets[hashedKey] == NULL) {
        return NULL;
    }
    objeto *bucket = buckets[hashedKey];
    while (bucket != NULL) {
        if (strcmp(bucket->clave, clave) == 0) {
			//printf("clave: %s - valor: %s\n",clave,(char *)bucket->valor);
            return bucket->valor;
        }
            bucket = bucket->siguiente;
    }
    return NULL;
}